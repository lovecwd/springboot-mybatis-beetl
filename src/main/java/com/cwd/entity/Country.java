package com.cwd.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import com.cwd.common.BaseEntity;

/**
 * @author chenweidong
 * @create 2017-04-14:48
 **/
@TableName(value = "country")
public class Country extends BaseEntity<Country>{
    private String countryName;

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }
}
