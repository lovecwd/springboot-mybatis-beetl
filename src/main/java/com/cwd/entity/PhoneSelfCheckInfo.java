package com.cwd.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import com.cwd.common.BaseEntity;

/**
 * @author chenweidong
 * @create 2017-04-21:54
 **/
@TableName(value = "mobile_self_check")
public class PhoneSelfCheckInfo extends BaseEntity<PhoneSelfCheckInfo>{

    private String country;
    private String mobileBrand;
    private String mobileModel;
    private String mobileVersion;
    private String supportSituation;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getMobileBrand() {
        return mobileBrand;
    }

    public void setMobileBrand(String mobileBrand) {
        this.mobileBrand = mobileBrand;
    }

    public String getMobileModel() {
        return mobileModel;
    }

    public void setMobileModel(String mobileModel) {
        this.mobileModel = mobileModel;
    }

    public String getMobileVersion() {
        return mobileVersion;
    }

    public void setMobileVersion(String mobileVersion) {
        this.mobileVersion = mobileVersion;
    }

    public String getSupportSituation() {
        return supportSituation;
    }

    public void setSupportSituation(String supportSituation) {
        this.supportSituation = supportSituation;
    }
}
