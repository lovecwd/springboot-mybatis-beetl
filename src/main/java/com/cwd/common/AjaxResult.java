package com.cwd.common;

import java.io.Serializable;

/**
 * Created by Administrator on 2017/4/7.
 */
public class AjaxResult implements Serializable {
    private boolean success = true;
    private Object data;//数据
    private Integer total;
    private String msg;//处理消息

    public AjaxResult() {
    }

    public AjaxResult(boolean success) {
        this.success = success;
    }
    public AjaxResult(boolean success,String msg) {
        this.success = success;
        this.msg = msg;
    }
    public AjaxResult(boolean success,Object data,Integer total) {
        this.success = success;
        this.data = data;
        this.total = total;
    }
    public AjaxResult(Object data,boolean success) {
        this.success = success;
        this.data = data;
    }
    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Object getData() {
        return data;
    }
    public void setData(Object data) {
        this.data = data;
    }


    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
