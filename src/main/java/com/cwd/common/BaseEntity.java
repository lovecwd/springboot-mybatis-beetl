package com.cwd.common;/**
 * Created by Administrator on 2017/4/16.
 */

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.util.Date;

/**
 * @author chenweidong
 * @create 2017-04-11:51
 **/
public class BaseEntity<T extends Model> extends Model<T> {
    @TableId(type = IdType.AUTO)
    Integer id;
    @TableField(value = "createTime")
    Date createTime;
    @TableField(value = "updateTime")
    Date updateTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    protected Serializable pkVal() {
        return this.id;
    }
}
