package com.cwd.controller;

import com.cwd.common.AjaxResult;
import com.cwd.entity.Country;
import com.cwd.entity.PhoneSelfCheckInfo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.*;

/**
 * @author 陈卫东
 * @create 2017-04-22:01
 **/
@RestController
@Controller
//@CrossOrigin("http://localhost")
public class PhoneSelfCheckController {

    /*public static String[] countries = {"美国","加拿大","墨西哥","日本","韩国","","",
            "","","","","","","","","","","","","","","","","","","","","","","",""};*/
    public static Set<String> countries = new HashSet<String>();
    static {
        Country country = new Country();
        List<Country> countriesList = country.selectAll();
        if(countriesList!= null & countriesList.size()>0){
            for(Country c : countriesList){
                countries.add(c.getCountryName());
            }
        }
        //countries
    }

    @GetMapping(value = "checkInfos")
    public Object getInfo(HttpServletResponse response){
        response.setHeader("Access-Control-Allow-Origin", "*");
        PhoneSelfCheckInfo p = new PhoneSelfCheckInfo();
        List<PhoneSelfCheckInfo> pss = p.selectAll();
        return new AjaxResult(pss,true);
    }


    @GetMapping(value = "selects")
    public Object getDistinctSelects(HttpServletResponse response){
        response.setHeader("Access-Control-Allow-Origin", "*");
        PhoneSelfCheckInfo p = new PhoneSelfCheckInfo();
        List<PhoneSelfCheckInfo> pss = p.selectAll();
        Map<String,Set<String>> map = new HashMap<String, Set<String>>();
        Set<String> mobileBrands = new HashSet<String>();
        Set<String> mobileModels = new HashSet<String>();
        Set<String> mobileVersions = new HashSet<String>();
        Set<String> supportSituations = new HashSet<String>();
        for(PhoneSelfCheckInfo info : pss){
            //countries.add(info.getCountry());
            mobileBrands.add(info.getMobileBrand());
            mobileModels.add(info.getMobileModel());
            mobileVersions.add(info.getMobileVersion());
            supportSituations.add(info.getSupportSituation());
            map.put("countries",countries);
            map.put("mobileBrands",mobileBrands);
            /*map.put("mobileModels",mobileModels);
            map.put("mobileVersions",mobileVersions);
            map.put("supportSituations",supportSituations);*/
        }
        return new AjaxResult(map,true);
    }
}
