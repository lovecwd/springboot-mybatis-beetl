package com.cwd.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.cwd.entity.PhoneSelfCheckInfo;

/**
 * @author cwd
 * @create 2017-04-21:56
 **/
public interface PhoneSelfCheckMapper extends BaseMapper<PhoneSelfCheckInfo> {
}
